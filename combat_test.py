import collections
import multiprocessing
import random

import characters
from core import gr
from engine.logger import Logger


# move to engine after it's done
class Combat(object):

    _ATTACK = 'attack'
    _BLOCK = 'block'

    _BATTLE_ACTION_LS = [_ATTACK,
                         _BLOCK]

    def __init__(self, player, mob):
        self.player = player
        self.mob = mob
        self.initiative_result = 0  # -1 goes the mob, 1 goes to player
        self.action_count = 0
        self.player_action = None
        self.mob_action = None

    def battle(self):
        Logger.log_it(message="%s attacks you! it has a [magical] [2 handed sword]" % self.mob.name,
                      message_type=gr.MOB_LOG_TYPE)

        # check that both player and mob are still alive
        while self.player.health > 0 and self.mob.health > 0:

            # the battle actions are calculated here
            self.mainBattleLoop()

            # has the battle ended yet?
            self.endBattleCheck()

    def battleInitiativeCheck(self):
        """ check initiative values to determine who goes first """
        # TODO this needs to be stochastic
        if self.action_count == 0:
            if self.player.initiative > self.mob.initiative:
                self.hand_off = 1  # hand off to the player
            else:
                self.hand_off = -1  # hand off to the mob

    def mainBattleLoop(self):
        """ capture and compute battle actions """
        try:
            self.battleActions()
        except multiprocessing.TimeoutError:  # player took too long to act
            self.hand_off = -1
            self.player.action_ls.append(None)

            if self.action_count in [0, 1]:
                print('\nAct faster!')
            elif self.action_count in [2, 3]:
                print("\nAren't you going to do something?")
            elif self.action_count in [4, 5]:
                print('\nAre you trying to die?')
            else:
                pass

    ## TODO 10.0 needs to be updated to the config value set by player
    #@utility.timeout(10.0)
    def battleActions(self):
        """ this does all the calculations related to relevant battle actions """

        # determine who strikes first
        self.battleInitiativeCheck()

        # get actions from player/mob
        self.actionCapture()

        # mob damage calculation
        mob_damage_dealt = self.calculateAttackDamage(base_damage=self.mob.damage,
                                                      lb=self.mob.lb_variance,
                                                      ub=self.mob.ub_variance,
                                                      initiative=self.mob.initiative)

        player_damage_dealt = self.calculateAttackDamage(base_damage=self.player.damage,
                                                         lb=self.player.lb_variance,
                                                         ub=self.player.ub_variance,
                                                         initiative=self.player.initiative)

        # player[block], mob[attack]
        if self.player_action == self._BLOCK and self.mob_action == self._ATTACK:

            damage_reflected = self.calculateBlockResult(damage_dealt=mob_damage_dealt,
                                                         defense=self.mob.defense)

            Logger.log_it(message="%s tried to attack you, but you blocked and retaliated for %s damage!" % (self.mob.name, damage_reflected),
                          message_type=gr.MOB_COMBAT_LOG_TYPE)

            self.mob.health -= damage_reflected

        # player[attack], mob[block]
        elif self.player_action == self._ATTACK and self.mob_action == self._BLOCK:

            damage_reflected = self.calculateBlockResult(damage_dealt=player_damage_dealt,
                                                         defense=self.player.defense)

            Logger.log_it(message="%s blocked your attack and retaliated for %s damage!" % (self.mob.name, damage_reflected),
                          message_type=gr.MOB_COMBAT_LOG_TYPE)

            self.player.health -= damage_reflected

        # player[attack], mob[attack]
        elif self.mob_action == self._ATTACK and self.player_action == self._ATTACK:

            self.player.health -= mob_damage_dealt
            Logger.log_it(message="%s attacks you for %s damage!" % (self.mob.name, mob_damage_dealt),
                          message_type=gr.MOB_COMBAT_LOG_TYPE)

            self.mob.health -= player_damage_dealt
            Logger.log_it(message="You struck %s for %s points of damage!" % (self.mob.name, player_damage_dealt),
                          message_type=gr.PLAYER_COMBAT_LOG_TYPE)

        # player[block], mob[block]
        elif self.mob_action == self._BLOCK and self.player == self._BLOCK:

            Logger.log_it(message="You both block, stare at each other funny, and nothing happens!",
                          message_type=gr.PLAYER_COMBAT_LOG_TYPE)

    def actionCapture(self):
        # action capture
        # player
        self.player_action = raw_input('> ').strip().lower()
        self.player.action_ls.append(self.player_action)

        # the mob needs to determine what action to take...
        # to start, let's do a simple count check for proof of concept
        self.mob_action = self.mobActionCalculator()

        if self.player_action not in self._BATTLE_ACTION_LS:
            Logger.log_it(message="You feel yourself slipping.",
                          message_type=gr.PLAYER_LOG_TYPE)

            Logger.log_it(message="%s takes advantage and hits you for %s damage!" % (self.mob.name, self.mob.offense),
                          message_type=gr.MOB_COMBAT_LOG_TYPE)

            self.player.health -= self.mob.offense
            self.endBattleCheck()

    def mobActionCalculator(self):

        counter_dct = {self._BLOCK: [self._ATTACK]
                       }
        mob_action = self._ATTACK

        if self.player.action_ls:

            counter = collections.Counter(self.player.action_ls)
            most_common = counter.most_common(3)[0][0]

            if most_common == self._ATTACK:
                mob_action = self._BLOCK
            else:
                mob_action = self._ATTACK

        return mob_action

    def calculateAttackDamage(self, base_damage, lb, ub, initiative):
        """ calculate how much damage the attack does """

        # check if ub or lb will occur
        if 1.4*random.random() + initiative >= 2:
            ub_range = range(ub+1)
            ub = random.choice(ub_range)
            damage_dealt = base_damage + ub
        else:
            lb_range = range(lb+1)
            lb = random.choice(lb_range)
            damage_dealt = base_damage - lb

        return damage_dealt

    def calculateBlockResult(self, damage_dealt, defense):

        # do they have a shield?

        # calculate damage pass off here
        damage_reflected = 0.5*float(damage_dealt - defense)

        return damage_reflected

    def endBattleCheck(self):
        """ determine if the battle has ended, and run post battle scripts if needed """
        self.action_count += 1

        if self.mob.health <= 0:
            Logger.log_it(
                message="You have slain %s!\nYou gained %s experience points." % (self.mob.name, self.mob.experience),
                message_type=gr.EXPERIENCE_GAIN_LOG_TYPE)
            self.player.experience += self.mob.experience

            # TODO check if level up occurs next

        if self.player.health <= 0:
            Logger.log_it(message="You can take no more. Your arms have been lopped off.\n"
                                  "Your entrails spew out beside your lifeless corpse.\n"
                                  "I knew you were weak, %s. Your journey ends here." % self.player.name,
                          message_type=gr.NARRATOR_LOG_TYPE)

    @staticmethod
    def combatCommandList():
        ls = ['attack', 'block', 'dodge', 'flee', 'parry', 'riposte']
        print(', '.join(ls))


player = characters.Player('Aldore')
mob = characters.Mobs()
mob.getMobDataByName('a goblin')

c = Combat(player, mob)
c.battle()
