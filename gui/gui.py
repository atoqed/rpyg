import Tkinter

from core import gr
from engine import config


class GameUI(object):
    """build the game UI"""

    # GLOBALS
    chat_log = None
    chat_log_input = None

    def __init__(self):
        self.length = '1280'
        self.width = '720'

        # root business
        self.root = Tkinter.Tk()
        self.root.title("pyrpg")
        self.root.geometry('%sx%s' % (self.length, self.width))

        # frame widgets
        # player_status_frame
        self.player_status_frame = Tkinter.Frame(self.root)
        self.player_status_frame.grid(row=0, column=0)

        # chat_frame
        self.chat_frame = Tkinter.Frame(self.root)
        self.chat_frame.grid(row=1, column=0)

        # start_game_frame
        self.start_game_frame = Tkinter.Frame(self.root)
        self.start_game_frame.grid(row=1, column=1)

        # widget vars / widget items
        # chat_frame
        self.chat_log_title = None
        self.scroll = None
        self.chat_log_submit = None

        # player_status_frame
        self.player_name_label = None
        self.player_name_var = Tkinter.StringVar()
        self.player_current_health_label = None
        self.player_current_health_var = Tkinter.IntVar()
        self.player_max_health_label = None
        self.player_max_health_var = Tkinter.IntVar()

        # bindings
        self.root.bind("<Return>", gr.LOGGER.receive)

    def build(self):
        """ put it all together"""
        self.compileChatFrame()
        self.compilePlayerStatusFrame()
        self.compileStartGameFrame()
        self.root.mainloop()

    def compileChatFrame(self):
        """build the chat log frame widget"""

        # chat log title
        self.chat_log_title = Tkinter.Label(self.chat_frame, text='Game Log')
        self.chat_log_title.grid(row=0, column=0, sticky=Tkinter.W)

        # chat log
        GameUI.chat_log = Tkinter.Text(self.chat_frame)
        GameUI.chat_log.config(state=Tkinter.DISABLED)
        GameUI.chat_log.tag_configure(tagName=gr.COMMAND_LOG_TYPE, foreground='purple')
        GameUI.chat_log.tag_configure(tagName=gr.NARRATOR_LOG_TYPE, foreground='#0000A0')
        GameUI.chat_log.tag_configure(tagName=gr.PLAYER_COMBAT_LOG_TYPE, foreground='red')
        GameUI.chat_log.tag_configure(tagName=gr.PLAYER_LOG_TYPE, foreground='black')
        GameUI.chat_log.tag_configure(tagName=gr.SYSTEM_LOG_TYPE, foreground='#FF00FF')
        GameUI.chat_log.grid(row=1, column=0, sticky=Tkinter.W)

        # scrollbar
        self.scroll = Tkinter.Scrollbar(self.chat_frame,
                                        orient=Tkinter.VERTICAL,
                                        command=GameUI.chat_log.yview)
        GameUI.chat_log.configure(yscrollcommand=self.scroll.set)
        self.scroll.config(command=GameUI.chat_log.yview)
        self.scroll.grid(row=1, column=1, sticky='nsew')

        # chat input
        GameUI.chat_log_input = Tkinter.Entry(self.chat_frame, background='gray')
        GameUI.chat_log_input.grid(row=2, sticky='nsew')
        GameUI.chat_log_input.focus()

        # chat submit
        self.chat_log_submit = Tkinter.Button(self.chat_frame,
                                              text='Submit',
                                              command=gr.LOGGER.receive)
        self.chat_log_submit.grid(row=2, column=0, columnspan=2, sticky=Tkinter.E)

    def compilePlayerStatusFrame(self):
        """build the player status frame widget"""

        # player_name
        self.player_name_label = Tkinter.Label(self.player_status_frame,
                                               textvariable=self.player_name_var)
        self.player_name_label.grid(row=0, column=0, sticky=Tkinter.W)

        # player_current_health
        self.player_current_health_label = Tkinter.Label(self.player_status_frame,
                                                         textvariable=self.player_current_health_var)
        self.player_current_health_label.grid(row=1, column=0, sticky=Tkinter.W)

        # player_max_health
        self.player_current_health_label = Tkinter.Label(self.player_status_frame,
                                                         textvariable=self.player_max_health_var)
        self.player_current_health_label.grid(row=1, column=0, sticky=Tkinter.E)

        self.player_name_var.set("Aldore")
        self.player_current_health_var.set(40)
        self.player_max_health_var.set(64)

    def compileStartGameFrame(self):
        start_button = Tkinter.Button(self.start_game_frame,
                                      text='Start Game')
        start_button.grid()
        start_button['command'] = lambda button=start_button: click(button)


def click(button):
    config.startThread()
    button.destroy()


if __name__ == '__main__':
    GameUI().build()
