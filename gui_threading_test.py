'''
# Run tkinter code in another thread
import Tkinter
import threading


class App(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.start()

    def callback(self):
        self.root.quit()

    def run(self):
        self.root = Tkinter.Tk()
        self.label_name = Tkinter.StringVar()
        self.label_name.set('testing')
        self.root.protocol("WM_DELETE_WINDOW", self.callback)

        label = Tkinter.Label(self.root, textvariable=self.label_name)
        label.pack()

        self.root.mainloop()


def updateLabel(label):
    label.set('name')
    return label



app = App()
print('Now we can continue running code while mainloop runs!')
'''

import time
import threading
from Tkinter import *
import ttk


def initialize_game():
    time.sleep(5)
    # put your stuff here


def start_config_thread(event=None):
    global submit_thread
    submit_thread = threading.Thread(target=initialize_game)
    submit_thread.daemon = True
    progressbar.start()
    submit_thread.start()
    root.after(20, check_config_thread)


def check_config_thread():
    if submit_thread.is_alive():
        root.after(20, check_config_thread)
    else:
        progressbar.stop()


root = Tk()
frame = ttk.Frame(root)
frame.pack()
progressbar = ttk.Progressbar(frame, mode='indeterminate')
progressbar.grid(column=1, row=0, sticky=W)

ttk.Button(frame, text="Check",
           command=lambda: start_config_thread()).grid(column=0, row=1, sticky=E)

root.mainloop()
