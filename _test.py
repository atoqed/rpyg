class Event(list):
    """Event subscription.

    A list of callable objects. Calling an instance of this will cause a
    call to each item in the list in ascending order by index.

    Example Usage:
    >>> def f(x):
    ...     print 'f(%s)' % x
    >>> def g(x):
    ...     print 'g(%s)' % x
    >>> e = Event()
    >>> e()
    >>> e.append(f)
    >>> e(123)
    f(123)
    >>> e.remove(f)
    >>> e()
    >>> e += (f, g)
    >>> e(10)
    f(10)
    g(10)
    >>> del e[0]
    >>> e(2)
    g(2)

    """
    def __call__(self, *args, **kwargs):
        for f in self:
            f(*args, **kwargs)

    def __repr__(self):
        return "Event(%s)" % list.__repr__(self)

if __name__ == "__main__":

    def f(x):
        z = 1
        print('f: end with %s' % int(x-z))

    def g(y):
        print('g: gave me %s' % y)

    e = Event()
    e()
    e.append(f)
    e(123)
    f(123)
    e.remove(f)
    e()
    e += (f, g)
    e(10)
    f(10)
    g(10)
    del e[0]
    e(2)
    g(2)

    print('my own experiment begins here')
    print('-----------------------------')

    def commands(command=None):
        print("the given command was: %s" % command)

    e = Event()
    command_ls = [commands('attack'), commands('help')]
    e.append(command_ls)
    #e()
    e.remove(command_ls)
    #e(commands)
    #e()
    e.append(commands)
    e('block')
    e('attack')
