class Events(object):
    """ this is where all data will be sent when calculating next actions to take """

    def __init__(self):
        # the command being executed in this packet
        self.command = None

        # the executor of the command
        self.current = None

        # the target(s) being impacted by the command
        self.target = None
