import uuid

from config import initializeGame
from core import gr
from commands import player_input


class Listener(object):
    """ this class will always be listening for commands the player types in """

    def __init__(self):
        self.listener_id = uuid.uuid4()

    @staticmethod
    def listen():
        if gr.GAME_SETUP is False:
            # in combat
            if gr.COMBAT_STATE is True:
                player_input()

            # not in combat
            if gr.COMBAT_STATE is False:
                player_input()
                pass





class DeathCheck(object):
    pass


class HealthManager(object):
    pass
