from core import gr
from engine.listener import Listener
import commands
import logger
from gui import gui


def runEngine():

    if gr.GAME_SETUP is True:
        # generate global command controller
        gr.COMMANDS = commands.Commands(command=None)

        # generate global logger controller
        gr.LOGGER = logger.MessageLogger()

        # generate game ui
        gr.GAME_UI = gui.GameUI()
        gr.GAME_UI.build()
        gr.GAME_SETUP = False

    # begin game loop
    while gr.PLAYER.current_health > 0:
        event = Listener.listen()
        continue

    print("Game Over.")


if __name__ == "__main__":
    runEngine()
