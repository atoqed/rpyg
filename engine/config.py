import threading
from random import randint

from characters import characters
from core import gr
from core import utility
from world import world


class Config(object):
    def __init__(self):

        # player input values
        self.difficulty_str = None
        self.length_str = None

        # actual config values
        self.difficulty = None
        self.length = None
        self.rarity_factor = None
        self.time_to_act = None
        self.generate_pdf = True

    def configureGame(self):
        if self.difficulty_str == 'easy':
            self.difficulty = 0.75
            self.time_to_act = 10
            self.rarity_factor = 2
        elif self.difficulty_str == 'normal':
            self.difficulty = 1
            self.time_to_act = 6
            self.rarity_factor = 1
        elif self.difficulty_str == 'hard':
            self.difficulty = 1.25
            self.time_to_act = 3
            self.rarity_factor = 0.75
        elif self.difficulty_str == 'savage':
            self.difficulty = 1.5
            self.time_to_act = 1.5
            self.rarity_factor = 0.50

        if self.length_str == 'short':
            self.length = 5
        elif self.length_str == 'normal':
            self.length = 10
        elif self.length_str == 'long':
            self.length = 20


def initializeGame():
    # init
    difficulty_correct_input = False

    gr.PLAYER = characters.Player('You')
    gr.CONFIG = Config()

    # introduction dialogue
    gr.LOGGER.bm(("build: %s" % gr.VERSION), gr.SYSTEM_LOG_TYPE)
    gr.LOGGER.transmit()
    gr.LOGGER.bm("Welcome to the most [savage] world in the world.", gr.NARRATOR_LOG_TYPE)
    gr.LOGGER.transmit()
    gr.LOGGER.bm("What name would you like to give your %s character?" % gr.ADJECTIVES[randint(0, 2)], gr.NARRATOR_LOG_TYPE)
    gr.LOGGER.transmit()

    # capture the player name
    gr.PLAYER.name = gr.LOGGER.capture()

    gr.LOGGER.bm("Hmm... %s...what a truly %s name." % (gr.PLAYER.name, gr.ADJECTIVES[randint(0, 2)]), gr.NARRATOR_LOG_TYPE)
    gr.LOGGER.transmit()

    # get user supplied difficulty
    while difficulty_correct_input is False:
        gr.LOGGER.bm(("Tell me, %s, how difficult do you want your journey to be? \n [easy, normal, hard]" % gr.PLAYER.name), gr.NARRATOR_LOG_TYPE)
        gr.LOGGER.transmit()
        difficulty = gr.LOGGER.capture()

        if difficulty not in ['easy', 'normal', 'hard', 'savage']:
            gr.LOGGER.bm(("You are clearly not very bright, %s. Provide me a valid response or risk immediate death." % gr.PLAYER.name), gr.NARRATOR_LOG_TYPE)
            gr.LOGGER.transmit()
        else:
            gr.CONFIG.difficulty_str = difficulty
            difficulty_correct_input = True

            if gr.CONFIG.difficulty_str == 'easy':
                gr.LOGGER.bm("I knew you were weak. Have you no soul? I shall find out and take it.", gr.NARRATOR_LOG_TYPE)
                gr.LOGGER.transmit()
            elif gr.CONFIG.difficulty_str == "normal":
                gr.LOGGER.bm("At least you didn't choose easy. You're still nothing though.", gr.NARRATOR_LOG_TYPE)
                gr.LOGGER.transmit()
            elif gr.CONFIG.difficulty_str == "hard":
                gr.LOGGER.bm("Good. You chose the only difficulty worth playing at. "
                             "There is one above this though you know...\n"
                             "I leave if off the list to keep the kiddos away.", gr.NARRATOR_LOG_TYPE)
                gr.LOGGER.transmit()
            elif gr.CONFIG.difficulty_str == "savage":
                gr.LOGGER.bm("Haha. You are quite clever. Now you must die.", gr.NARRATOR_LOG_TYPE)
                gr.LOGGER.transmit()

    # load game assets
    gr.LOGGER.bm("Loading game assets, and generating world...", gr.SYSTEM_LOG_TYPE)
    gr.LOGGER.transmit()

    # load the zone/mob/item data into the game
    utility.loadAssetsFromJson()
    world.generateWorld()
    gr.LOGGER.bm("Loading and generation complete.", gr.SYSTEM_LOG_TYPE)

    # load player into game world
    print(gr.ZONE_DATA)
    world.loadPlayer(zone_id=gr.ZONE_DATA[0], load_location=None)
    print(gr.PLAYER)


def startThread():
    global submit_thread
    submit_thread = threading.Thread(target=initializeGame)
    submit_thread.daemon = True
    submit_thread.start()
