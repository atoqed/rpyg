class ActionPacket(object):

    def __init__(self):
        self.target = None
        self.regen = None
        self.dot = None
        self.skill = None
        self.physical_damage = None
        self.fire_damage = None
        self.arcane_damage = None
        self.poison_damage = None
        self.water_damage = None


class ExecuteActions(ActionPacket):
    pass
