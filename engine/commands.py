from core import gr


def player_input():
    player_gives = raw_input("> ")

    c = Commands(player_gives)
    c.runCommand()


class Commands(object):

    # misc commands
    _HELP = '/help'
    _MOVE = '/move'
    _STATUS = '/status'
    _START_CHAT = '/start chat'
    _COMMAND_LS = [_HELP, _MOVE, _START_CHAT, _STATUS]

    # movement commands
    _NORTH = '/north'
    _SOUTH = '/south'
    _EAST = '/east'
    _WEST = '/west'
    _MOVEMENT_COMMANDS = [_NORTH, _SOUTH, _EAST, _WEST]

    def __init__(self, command):
        self.command = command

    def runCommand(self):

        # check if command is valid
        if self.command not in self._COMMAND_LS:
            message = ("'%s' is not a valid command. Type 'help' to see a list of valid commands." % self.command)
            return message, True

        if self.command in self._COMMAND_LS:
            # the help command
            if self.command == self._HELP:
                message = self.getCommands(self)
                return message, True

            # the move command
            if self.command == self._MOVE:
                print("entering movement mode")
                gr.GAME_STATE = 'movement'

                # open key captures

                # check if move is valid

                # execute player movement

            # the status command
            if self.command == self._STATUS:
                message = (' Your current status:'
                                  '\n Combat: %s'
                                  '\n Targets: TBD' %
                                  (gr.COMBAT_STATE
                                   ))
                return message, True

            # movement commands
            if self.command in self._MOVEMENT_COMMANDS:
                # execute the movement command if possible
                return None, False

    @staticmethod
    def getCommands(self):
        """ give player a list of valid commands """

        message = " Available commands:"
        for c in self._COMMAND_LS:
            message += '\n ' + c

        return message


