import copy
import threading

import Tkinter

from core import gr


class MessageLogger(object):
    """
    this class controls how messages are sent to the chat log
    fn: bm: update gr with the message and message type. this MUST be done prior to sending
    fn: receive: called by pressing enter or submit while the chat input is active
    fn: transmit: called be receive and does the actual work of sending the message to the chat log
    fn: capture: called when player input is needed to be captured and/or stored

    ex:

    """

    def __init__(self):
        self.message = None
        self.message_type = None
        self.received = False
        self.stored = None

    def bm(self, message, message_type):
        """update the GR with a new message so it can be sent using receive/transmit"""

        self.message_type = message_type

        if self.message_type == gr.PLAYER_LOG_TYPE:
            self.message = '[You]:  ' + message
        elif self.message_type == gr.SYSTEM_LOG_TYPE:
            self.message = '[System]:  ' + message
        elif self.message_type == gr.NARRATOR_LOG_TYPE:
            self.message = '[Narrator]:  ' + message
        elif self.message_type == gr.COMMAND_LOG_TYPE:
            self.message = '[Command]:  ' + message
        else:
            self.message = message

    def transmit(self):
        """transmit the message to the chat log"""
        print("transmitting...")
        print("transmit_message: %s" % self.message)
        print("transmit_message_type: %s" % self.message_type)
        if self.message:
            gr.GAME_UI.chat_log.config(state=Tkinter.NORMAL)
            gr.GAME_UI.chat_log.insert(Tkinter.END, "\n" + self.message)
            gr.GAME_UI.chat_log_input.delete(0, Tkinter.END)

            # attempt to find a message type for coloring
            if self.message_type is not None:
                pattern = '[' + self.message_type + ']:'
                gr.GAME_UI.chat_log.color_message_type(pattern=pattern, tag=self.message_type)

            gr.GAME_UI.chat_log.see("end")
            gr.GAME_UI.chat_log.config(state=Tkinter.DISABLED)
        else:
            print('attempted to transmit an empty message')
            gr.GAME_UI.chat_log.config(state=Tkinter.NORMAL)
            gr.GAME_UI.chat_log_input.delete(0, Tkinter.END)
            gr.GAME_UI.chat_log.see("end")
            gr.GAME_UI.chat_log.config(state=Tkinter.DISABLED)

        # reset variables after transmitting
        self.message = None
        self.message_type = None

        print("should be reset to None...")
        print("transmit_message: %s" % self.message)
        print("transmit_message_type: %s" % self.message_type)

        print("======"*5)

    def receive(self, event=None):
        """use to capture player input"""

        #is_command = True

        # wait for player response
        self.message = gr.GAME_UI.chat_log_input.get()
        print('the message', self.message)

        # when message is found
        if self.message:
            self.message_type = gr.PLAYER_LOG_TYPE
            # execute command
            if self.message[0] == '/':
                gr.COMMANDS.command = self.message
                self.message, continue_command = gr.COMMANDS.runCommand()
                if continue_command is True:
                    self.message_type = gr.COMMAND_LOG_TYPE
                    gr.LOGGER.bm(self.message, self.message_type)
                    self.transmit()
            else:
                # keep last thing typed by player in memory, currently overwrite each time
                # do not store commands
                if self.message_type == gr.PLAYER_LOG_TYPE:
                    self.stored = copy.deepcopy(self.message)

                # send the non-command message
                if self.message is None:
                    gr.LOGGER.bm(self.stored, self.message_type)
                else:
                    gr.LOGGER.bm(self.message, self.message_type)
                self.transmit()
                #is_command = False
                self.received = True

        # this recursion is called if the user supplies a command instead of a response during a capture call
        #if not self.message and is_command is True:
        #    gr.GAME_UI.root.after(1000, self.receive())

    def capture(self):
        """whenever a player value needs to be stored
           works by keeping track of whether the value has been stored or not"""

        self.received = False
        self.message_type = gr.PLAYER_LOG_TYPE

        # wait for keypress
        while gr.LOGGER.received is False:
            continue

        return gr.LOGGER.stored
