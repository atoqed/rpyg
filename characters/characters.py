

class Character(object):
    """ core character """

    # attributes
    _NAME_ATTR = 'name'
    _LEVEL_ATTR = 'level'
    _EXPERIENCE = 'experience'
    _HEALTH_ATTR = 'health'
    _HEALTH_REGEN_ATTR = 'health_regen'
    _MANA_ATTR = 'mana'
    _MANA_REGEN_ATTR = 'mana_regen'
    _DAMAGE_ATTR = 'damage'
    _SPEED_ATTR = 'speed'
    _INITIATIVE_ATTR = 'initiative'
    _LB_VARIANCE_ATTR = 'lb_variance'
    _UB_VARIANCE_ATTR = 'ub_variance'
    _AI_TYPE_ATTR = 'ai_type'
    _OFFENSE_ATTR = 'offence'
    _DEFENSE_ATTR = 'defense'
    _ATTRIBUTES_ATTR = 'attributes'
    _INVENTORY_ATTR = 'inventory'

    def __init__(self):
        # attributes
        self.name = None
        self.level = None
        self.experience = None
        self.max_health = None
        self.health_regen = None
        self.mana = None
        self.mana_regen = None
        self.damage = None
        self.speed = None
        self.initiative = None
        self.lb_variance = None
        self.ub_variance = None
        self.ai_type = None
        self.offense = None
        self.defense = None
        self.attributes = None
        self.inventory = None

        self.action_ls = []


class Mobs(Character):
    """ the mobs character type """
    def __init__(self):
        Character.__init__(self)

    def getMobDataByName(self, mob_name):

        # retrieve record from dict
        mob_dct = self.mobData(mob_name)

        # load data into Mob object
        self.name = mob_dct[self._NAME_ATTR]
        self.level = mob_dct[self._LEVEL_ATTR]
        self.experience = mob_dct[self._EXPERIENCE]
        self.health = mob_dct[self._HEALTH_ATTR]
        self.health_regen = mob_dct[self._HEALTH_REGEN_ATTR]
        self.mana = mob_dct[self._MANA_ATTR]
        self.mana_regen = mob_dct[self._MANA_REGEN_ATTR]
        self.damage = mob_dct[self._DAMAGE_ATTR]
        self.speed = mob_dct[self._SPEED_ATTR]
        self.initiative = mob_dct[self._INITIATIVE_ATTR]
        self.lb_variance = mob_dct[self._LB_VARIANCE_ATTR]
        self.ub_variance = mob_dct[self._UB_VARIANCE_ATTR]
        self.ai_type = mob_dct[self._AI_TYPE_ATTR]
        self.offense = mob_dct[self._OFFENSE_ATTR]
        self.defense = mob_dct[self._DEFENSE_ATTR]
        self.attributes = mob_dct[self._ATTRIBUTES_ATTR]
        self.inventory = mob_dct[self._INVENTORY_ATTR]

    @classmethod
    def mobData(cls, mob_name):
        # mob names
        a_goblin_MOB = "a goblin"
        A_goblin_MOB = "A goblin"

        # i know they are not mobile objects...yet ;)
        mobs = {a_goblin_MOB:
                {cls._NAME_ATTR: a_goblin_MOB,  # the mob name
                 cls._LEVEL_ATTR: 1,  # the level of the mob
                 cls._EXPERIENCE: 5,  # how much exp the mob awards
                 cls._HEALTH_ATTR: 19,  # health of mob
                 cls._HEALTH_REGEN_ATTR: 2,  # how much health is regenerates per 3 actions
                 cls._MANA_ATTR: 0,  # how much mana the mob has
                 cls._MANA_REGEN_ATTR: 0,  # how much mana the mob regenerates per 3 actions
                 cls._DAMAGE_ATTR: 8,  # damage per attack
                 cls._SPEED_ATTR: 1,  # attacks per action
                 cls._INITIATIVE_ATTR: 1,  # who attacks first
                 cls._LB_VARIANCE_ATTR: 2,  # the lower bound in damage output: 8-2=6 min damage
                 cls._UB_VARIANCE_ATTR: 1,  # the upper bound in damage output: 8+1=9 max damage
                 cls._AI_TYPE_ATTR: 0,  # 0-10 where 0 is dumb and 10 is smart
                 cls._INVENTORY_ATTR: {},  # the items carried by the mob
                 cls._OFFENSE_ATTR: 5,  # how likely you are to hit
                 cls._DEFENSE_ATTR: 0,  # how likely you are to reduce damage taken
                 cls._ATTRIBUTES_ATTR: []  # a list of attributes
                 },
                A_goblin_MOB:
                {cls._NAME_ATTR: A_goblin_MOB,
                 cls._LEVEL_ATTR: 1,
                 cls._EXPERIENCE: 8,
                 cls._HEALTH_ATTR: 26,
                 cls._HEALTH_REGEN_ATTR: 2.5,
                 cls._DAMAGE_ATTR: 10,
                 cls._SPEED_ATTR: 1.1,
                 cls._INITIATIVE_ATTR: 1.1,
                 cls._LB_VARIANCE_ATTR: 3,
                 cls._UB_VARIANCE_ATTR: 2,
                 cls._AI_TYPE_ATTR: 1,
                 cls._INVENTORY_ATTR: {},
                 cls._OFFENSE_ATTR: 6,
                 cls._DEFENSE_ATTR: 2,
                 cls._ATTRIBUTES_ATTR: []
                 }
                }

        return mobs[mob_name]


class Player(Character):
    """ player data """
    def __init__(self, name):
        Character.__init__(self)

        # attributes
        self.name = name
        self.zone = None
        self.current_tile = [0, 0]
        self.level = 1
        self.experience = 0
        self.max_health = 64
        self.current_health = 64
        self.health_regen = 5
        self.max_mana = 50
        self.current_mana = 50
        self.mana_regen = 5
        self.damage = 8
        self.speed = 1.2
        self.initiative = 2.5
        self.lb_variance = 2
        self.ub_variance = 2
        self.ai_type = None
        self.offense = 3
        self.defense = 3
        self.attributes = None
        self.inventory = None
