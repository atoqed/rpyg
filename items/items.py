class Weapon(object):
    def __init__(self, weapon_id):
        self.id = weapon_id

    @classmethod
    def WeaponDirectory(cls):
        weapons = {0: {"Name": "Cracked Sword",
                       "Damage": 8,  # damage per attack
                       "Speed": 35,  # factors into initiative ?
                       "Attributes": []  # a list of weapon attributes
                       }
                   }
