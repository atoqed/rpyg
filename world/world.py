from itertools import repeat
import random

from core import gr
from engine import logger


def getRandomId(id_ls):
    """ return a random id from the provided list """
    return random.choice(id_ls)


def getRandomTile(tiles, width=None, safe_zone_height=None):
    """ return a random tile from the list, excluding None
        supports blocker height """

    # remove None values
    tiles = [t for t in tiles if t is not None]
    print(tiles)
    # remove tiles from selection if width and safe_zone_height are specified
    if (safe_zone_height is not None and width is not None
            and isinstance(safe_zone_height, int) and isinstance(width, int)):
        szl = [width - i - 1 for i in range(safe_zone_height)]  # safe zone list
        tiles = [t for t in tiles if t[0] not in szl]
        rt = random.choice(tiles)  # random tile
        pass
    else:
        rt = random.choice(tiles)

    return rt


def generateWorld():
    """ loop through and generate all the zones """

    for id in gr.ZONE_DATA:
        zone = gr.ZONE_DATA[id]
        '''
        z = [ [B, M, E], [I, M, M], [P, V, I] ]
        stacks "left to right / top to bottom" for example, the actual structure is like this
        
           0  1  2
        0 [B, M, E]
        1 [I, M, M]
        2 [P, V, I]
        
        E = Exit/Entrance
        P = Player
        M = Mob
        B = Boss
        I = Impassable terrain
        V = Void (nothing in tile)
        
        z[0][1] = M should give the tile corresponding to row 0 column 1
        '''

        # generate the empty world based on zone dimensions, ex: [[], [], []]
        remaining_tiles = []
        generated = [[] for _ in repeat(None, zone['length'])]
        for i, _ in enumerate(generated):
            generated[i] = [None for _ in repeat(None, zone['width'])]

        # populate the zone with characteristics
        for w in range(zone['width']-1, -1, -1):
            for l in range(zone['length']-1, -1, -1):
                remaining_tiles.append([w, l])

        # remove player starting location/zone exit, entrance from possible tile placement
        if zone['starting_coordinates'] is not None:
            generated[zone['starting_coordinates'][0]][zone['starting_coordinates'][1]] = {'type': 'start', 'id': None}
            remaining_tiles[remaining_tiles.index(zone['starting_coordinates'])] = None

        generated[zone['exit_coordinates']['E1'][0]][zone['exit_coordinates']['E1'][1]] = {'type': 'exit/entrance', 'id': None}
        remaining_tiles[remaining_tiles.index(zone['exit_coordinates']['E1'])] = None

        # place the mobs
        for i in range(zone['num_mobs']):
            rsm = getRandomId(zone['mob_ids'])  # randomly selected mob
            rst = getRandomTile(remaining_tiles,
                                width=zone['width'],
                                safe_zone_height=zone['safe_zone_height'])

            generated[rst[0]][rst[1]] = {'type': 'mob', 'id': rsm}
            remaining_tiles[remaining_tiles.index(rst)] = None

        # place the boss(es)
        for i in range(zone['num_bosses']):
            rsb = getRandomId(zone['boss_ids'])  # randomly selected boss
            rst = getRandomTile(remaining_tiles,
                                width=zone['width'],
                                safe_zone_height=zone['safe_zone_height'])

            generated[rst[0]][rst[1]] = {'type': 'boss', 'id': rsb}
            remaining_tiles[remaining_tiles.index(rst)] = None

        # place terrain blockers
        # this needs to be refined, currently the player could get blocked in by blockers placed in a certain way
        for i in range(zone['terrain_blockers']):
            rst = getRandomTile(remaining_tiles)
            generated[rst[0]][rst[1]] = {'type': 'blocker', 'id': None}
            remaining_tiles[remaining_tiles.index(rst)] = None

        # finally, populate all remaining tiles with void
        for tile in remaining_tiles:
            if tile is not None:
                generated[tile[0]][tile[1]] = {'type': 'void', 'id': None}
                remaining_tiles[remaining_tiles.index(tile)] = None

        zone['generated'] = generated

        # [debug printing] for testing, print current game state
        #for tilerow in generated:
        #    print(tilerow)


def loadPlayer(zone_id, load_location=None):
    """ this function loads the player into the zone"""

    gr.LOGGER.bm("Loading %s..." % zone_id['name'], gr.SYSTEM_LOG_TYPE)
    gr.LOGGER.transmit()

    gr.PLAYER.zone = zone_id['name']

    if load_location is None:
        gr.PLAYER.current_tile = zone_id['starting_coordinates']
    # this part is used when the player exits/enters a zone
    if load_location is not None:
        gr.PLAYER.current_tile = load_location

    gr.LOGGER.bm("You have entered %s." % zone_id['name'], gr.SYSTEM_LOG_TYPE)
    gr.LOGGER.transmit()
