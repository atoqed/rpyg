import copy
import threading

import Tkinter


class GUI(object):
    """build the game UI"""

    # GLOBALS
    chat_log = None
    chat_log_input = None

    def __init__(self):

        # root business
        self.root = Tkinter.Tk()
        self.root.title("a chat log")

        # chat_frame
        self.chat_frame = Tkinter.Frame(self.root)
        self.chat_frame.grid(row=1, column=0)
        self.chat_log_title = None
        self.scroll = None
        self.chat_log_submit = None

        # bindings
        self.root.bind("<Return>", LOGGER.receive)

    def build(self):
        """ put it all together"""
        self.compileChatFrame()
        self.root.mainloop()

    def compileChatFrame(self):
        """build the chat log frame widget"""

        # chat log title
        self.chat_log_title = Tkinter.Label(self.chat_frame, text='Chat Log')
        self.chat_log_title.grid(row=0, column=0, sticky=Tkinter.W)

        # chat log
        GUI.chat_log = Tkinter.Text(self.chat_frame)
        GUI.chat_log.config(state=Tkinter.DISABLED)
        GUI.chat_log.tag_configure(tagName=COMMAND_LOG_TYPE, foreground='purple')
        GUI.chat_log.tag_configure(tagName=NARRATOR_LOG_TYPE, foreground='#0000A0')
        GUI.chat_log.tag_configure(tagName=PLAYER_LOG_TYPE, foreground='black')
        GUI.chat_log.tag_configure(tagName=SYSTEM_LOG_TYPE, foreground='#FF00FF')
        GUI.chat_log.grid(row=1, column=0, sticky=Tkinter.W)

        # scrollbar
        self.scroll = Tkinter.Scrollbar(self.chat_frame,
                                        orient=Tkinter.VERTICAL,
                                        command=GUI.chat_log.yview)
        GUI.chat_log.configure(yscrollcommand=self.scroll.set)
        self.scroll.config(command=GUI.chat_log.yview)
        self.scroll.grid(row=1, column=1, sticky='nsew')

        # chat input
        GUI.chat_log_input = Tkinter.Entry(self.chat_frame, background='gray')
        GUI.chat_log_input.grid(row=2, sticky='nsew')
        GUI.chat_log_input.focus()

        # chat submit
        self.chat_log_submit = Tkinter.Button(self.chat_frame,
                                              text='Submit',
                                              command=LOGGER.receive)
        self.chat_log_submit.grid(row=2, column=0, columnspan=2, sticky=Tkinter.E)


class Logger(object):
    """
    this class controls how messages are sent to the chat log
    fn: bm: update gr with the message and message type. this MUST be done prior to sending
    fn: receive: called by pressing enter or submit while the chat input is active
    fn: transmit: called be receive and does the actual work of sending the message to the chat log
    fn: capture: called when player input is needed to be captured and/or stored

    ex:

    """

    def __init__(self):
        self.message = None
        self.message_type = None
        self.received = False
        self.stored = None

    def bm(self, message, message_type):
        """update the GR with a new message so it can be sent using receive/transmit"""

        self.message_type = message_type

        if self.message_type == PLAYER_LOG_TYPE:
            self.message = '[You]:  ' + message
        elif self.message_type == SYSTEM_LOG_TYPE:
            self.message = '[System]:  ' + message
        elif self.message_type == NARRATOR_LOG_TYPE:
            self.message = '[Narrator]:  ' + message
        elif self.message_type == COMMAND_LOG_TYPE:
            self.message = '[Command]:  ' + message
        else:
            self.message = message

    def transmit(self):
        """transmit the message to the chat log"""
        print("transmitting...")
        print("transmit_message: %s" % self.message)
        print("transmit_message_type: %s" % self.message_type)
        if self.message:
            GAME_UI.chat_log.config(state=Tkinter.NORMAL)
            GAME_UI.chat_log.insert(Tkinter.END, "\n" + self.message)
            GAME_UI.chat_log_input.delete(0, Tkinter.END)

            # attempt to find a message type for coloring
            if self.message_type is not None:
                pattern = '[' + self.message_type + ']:'
                GAME_UI.chat_log.color_message_type(pattern=pattern, tag=self.message_type)

            GAME_UI.chat_log.see("end")
            GAME_UI.chat_log.config(state=Tkinter.DISABLED)
        else:
            print('attempted to transmit an empty message')
            GAME_UI.chat_log.config(state=Tkinter.NORMAL)
            GAME_UI.chat_log_input.delete(0, Tkinter.END)
            GAME_UI.chat_log.see("end")
            GAME_UI.chat_log.config(state=Tkinter.DISABLED)

        # reset variables after transmitting
        self.message = None
        self.message_type = None

        print("should be reset to None...")
        print("transmit_message: %s" % self.message)
        print("transmit_message_type: %s" % self.message_type)

        print("======"*5)

    def receive(self, event=None):
        """use to capture player input"""

        # wait for player response
        self.message = GAME_UI.chat_log_input.get()

        # when message is found
        if self.message:
            self.message_type = PLAYER_LOG_TYPE
            # execute command
            if self.message[0] == '/':
                COMMANDS.command = self.message
                self.message, continue_command = COMMANDS.runCommand()
                if continue_command is True:
                    self.message_type = COMMAND_LOG_TYPE
                    LOGGER.bm(self.message, self.message_type)
                    self.transmit()
            else:
                # keep last thing typed by player in memory, currently overwrite each time
                # do not store commands
                if self.message_type == PLAYER_LOG_TYPE:
                    self.stored = copy.deepcopy(self.message)

                # send the non-command message
                if self.message is None:
                    LOGGER.bm(self.stored, self.message_type)
                else:
                    LOGGER.bm(self.message, self.message_type)
                self.transmit()
                self.received = True

    def capture(self):
        """whenever a player value needs to be stored
           works by keeping track of whether the value has been stored or not"""

        self.received = False
        self.message_type = PLAYER_LOG_TYPE

        # wait for keypress
        while LOGGER.received is False:
            continue

        return LOGGER.stored


class Commands(object):

    # commands
    _HELP = '/help'
    _STATUS = '/status'
    _START_CHAT = '/start chat'
    _COMMAND_LS = [_HELP, _STATUS, _START_CHAT]

    def __init__(self, command):
        self.command = command

    def runCommand(self):

        # check if command is valid
        if self.command not in self._COMMAND_LS:
            message = ("'%s' is not a valid slash command. Type '/help' to see a list of commands." % self.command)
            return message, True

        if self.command in self._COMMAND_LS:
            # the help command
            if self.command == self._HELP:
                message = self.getCommands(self)
                return message, True

            # the status command
            if self.command == self._STATUS:
                message = (' Your current status:'
                           '\n Combat: %s'
                           '\n Targets: TBD' %
                           (COMBAT_STATE
                            ))
                return message, True

            # the start chat command
            if self.command == self._START_CHAT:
                startThread()
                return LOGGER.message, False

    @staticmethod
    def getCommands(self):
        """ give player a list of valid commands """

        message = " Available commands:"
        for c in self._COMMAND_LS:
            message += '\n ' + c

        return message


def haveAChat():
    # send some data to the chat widget

    LOGGER.bm("Welcome to the chat log!", NARRATOR_LOG_TYPE)
    LOGGER.transmit()

    # ask a question
    LOGGER.bm("How are you feeling today?", NARRATOR_LOG_TYPE)
    LOGGER.transmit()

    user_feels = LOGGER.capture()
    
    LOGGER.bm(("I see you feel %s...that's either good, bad, or neither. \n Aren't I smart?" % user_feels), NARRATOR_LOG_TYPE)
    LOGGER.transmit()

    LOGGER.bm("Well, it was nice chatting! Have a great day!", NARRATOR_LOG_TYPE)
    LOGGER.transmit()


def startThread():
    global submit_thread
    submit_thread = threading.Thread(target=haveAChat)
    submit_thread.daemon = True
    submit_thread.start()


if __name__ == '__main__':
    # core stuff
    PLAYER = None
    COMBAT_STATE = False

    # logging stuff
    COMMAND_LOG_TYPE = 'Command'
    NARRATOR_LOG_TYPE = 'Narrator'
    PLAYER_LOG_TYPE = 'Player'
    SYSTEM_LOG_TYPE = 'System'

    # build the classes
    COMMANDS = Commands(command=None)
    LOGGER = Logger()
    GAME_UI = GUI()
    GAME_UI.build()
