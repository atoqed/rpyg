import functools
import json
import multiprocessing.pool
import os

import gr


def timeout(max_timeout):
    """Timeout decorator, parameter in seconds."""
    def timeout_decorator(item):
        """Wrap the original function."""
        @functools.wraps(item)
        def func_wrapper(self, *args, **kwargs):
            """Closure for function."""
            pool = multiprocessing.pool.ThreadPool(processes=1)
            async_result = pool.apply_async(item, (self,) + args, kwargs)
            # raises a TimeoutError if execution exceeds max_timeout
            return async_result.get(max_timeout)
        return func_wrapper
    return timeout_decorator


def loadAssetsFromJson():
    """ load requested json assets """

    # paths
    character_path = os.path.normpath(os.getcwd() + os.sep + os.pardir) + '\characters\characters.json'
    item_path = os.path.normpath(os.getcwd() + os.sep + os.pardir) + '\items\items.json'
    zone_path = os.path.normpath(os.getcwd() + os.sep + os.pardir) + '\world\zones.json'

    # load the data into gr
    with open(zone_path) as json_data:
        gr.ZONE_DATA = json.load(json_data)

    # convert unicode ids to int for convenience
    for i, item in enumerate(gr.ZONE_DATA):
        gr.ZONE_DATA[int(item)] = gr.ZONE_DATA.pop(item)
