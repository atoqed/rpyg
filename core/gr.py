# gr for global resources
# core stuff
COMMANDS = None
CONFIG = None
GAME_UI = None
LOGGER = None
PLAYER = None

# game start
GAME_SETUP = True
TUTORIAL = False

# special fun words
ADJECTIVES = ['pathetic', 'interesting', 'wonderful', 'cowardly', 'weak', 'malignant', 'wild']

# logging stuff
COMMAND_LOG_TYPE = 'Command'
EXPERIENCE_GAIN_LOG_TYPE = 'Experience Gain Log'
MOB_LOG_TYPE = 'Mob'
MOB_COMBAT_LOG_TYPE = 'Mob Combat'
NARRATOR_LOG_TYPE = 'Narrator'
PLAYER_LOG_TYPE = 'Player'
PLAYER_COMBAT_LOG_TYPE = 'Player Combat'
SYSTEM_LOG_TYPE = 'System'
ZONE_LOG_TYPE = 'Zone'

# combat state
COMBAT_STATE = False
GAME_STATE = None

# json data
ITEM_DATA = None
MOB_DATA = None
ZONE_DATA = None

# version
VERSION = '0.0.1.1'
